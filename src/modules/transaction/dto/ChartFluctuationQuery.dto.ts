import { Overtime } from '../transaction.enum';

export interface ChartFluctuationQueryDTO {
  overtime: Overtime;
  slug: string;
}
