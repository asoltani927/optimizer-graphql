import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as helmet from "helmet";
import CorsConfig from "./config/app/cors.config";
import AppConfig from "./config/app/app.config";
import * as compression from "compression"
import * as rateLimit from "express-rate-limit"
import * as nocache from "nocache"
import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  //Get app config
  const appConfig: AppConfig = app.get(AppConfig);

  //Get cors config
  // const corsConfig: CorsConfig = app.get(CorsConfig);

  // app.enableCors({
  //   credentials: corsConfig.credentials,
  //   methods: corsConfig.methods,
  //   origin: corsConfig.origin,
  // });
  //
  // app.use(nocache())
  //
  // app.use(compression())

  // app.use(bodyParser.json({ limit: '50mb' }));

  app.use(
      (rateLimit as any)({
        windowMs: 60000, // 1 minutes
        max: appConfig.rateLimit, // limit each IP to 50 requests per windowMsmessage:
        // 'Too many accounts created from this IP, please try again after an hour',
        // standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
        // legacyHeaders: false, // Disable the `X-RateLimit-*` headers
      }),
  )
  // app.use(helmet);


  app.setGlobalPrefix('api');


  await app.listen(appConfig.port);
}
bootstrap().then(r => {

});
