import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { GasPriceResolvers } from './gas-price.resolvers';
import { GasPriceService } from './gas-price.service';
import { GasPriceSchedule } from './schedule/GasPrice.schedule';
import {GasPriceGateway} from "./gas-price.gateway";

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'GAS_PRICE_SERVER',
        transport: Transport.REDIS,
        options: {
          url: process.env.REDIS_URL,
        },
      },
    ]),
  ],
  providers: [GasPriceService, GasPriceResolvers, GasPriceSchedule, GasPriceGateway],
})
export class GasPriceModule {}
