import {Logger} from '@nestjs/common';
import {
    WebSocketGateway,
    WebSocketServer,
    OnGatewayConnection,
    OnGatewayInit,
    OnGatewayDisconnect,
} from '@nestjs/websockets';

@WebSocketGateway(4001)
export class AppGateway
    implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private readonly logger: Logger = new Logger('AppGateway');

    @WebSocketServer()
    wss;

    afterInit(server: any) {
        this.logger.log('Initialized');
    }

    handleConnection(client) {
        this.logger.log('Client connected: ' + client.id);
    }

    handleDisconnect(client) {
        this.logger.log('Client disconnected: ' + client.id);
    }
}
