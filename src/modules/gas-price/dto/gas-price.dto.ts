export interface GasPriceDTO {
  fast: number;
  slow: number;
  standard: number;
}
