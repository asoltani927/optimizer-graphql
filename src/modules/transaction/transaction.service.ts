import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ChartFluctuationQueryDTO } from './dto/ChartFluctuationQuery.dto';

@Injectable()
export class TransactionService {
  constructor(
    @Inject('SERVICES')
    private readonly transaction_server: ClientProxy,
  ) {}

  getFluctuationOfTransaction(input: ChartFluctuationQueryDTO) {
    return this.transaction_server.send('get_fluctuation_transaction_chart', input);
  }

}
