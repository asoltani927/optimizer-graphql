import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MarketResolvers } from './market.resolvers';
import { MarketService } from './market.service';

@Module({
  imports: [
    ClientsModule.register([
      {
          name: 'MARKET_SERVER',
          transport: Transport.REDIS,
          options:{
            url: process.env.REDIS_URL,
          }
      }
    ])
  ],
  providers: [
    MarketResolvers,
    MarketService
  ]
})
export class MarketModule {}
