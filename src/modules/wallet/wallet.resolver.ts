import {Resolver, Mutation, Args} from '@nestjs/graphql';
import {WalletService} from './wallet.service';

@Resolver()
export class WalletResolver {
    constructor(private readonly walletService: WalletService) {
    }

    @Mutation()
    async connectWallet(@Args('wallet_address') walletAddress: string) {
        return this.walletService.connect({
            wallet_address: walletAddress
        });
    }

    @Mutation()
    async walletPortfolio(@Args('wallet_address') walletAddress: string,
                          @Args('wallet_collections') walletCollections: [string],
                          @Args('collections_balances') collectionsBalances: [number]) {
        return this.walletService.portfolio({
            wallet_address: walletAddress,
            wallet_collections: walletCollections,
            collections_balances: collectionsBalances,
        });
    }

    //
    // @Query({ name: 'wallet' })
    // findOne(@Args('id', { type: () => Int }) id: number) {
    //   return this.walletService.findOne(id);
    // }
}
