import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export default class AppConfig {
  constructor(private configService: ConfigService) {}

  get name(): string {
    return this.configService.get<string>('APP_NAME') || 'Nest App';
  }

  get port(): number {
    return this.configService.get<number>('APP_PORT') || 3000;
  }

  get IsDebugMode(): boolean {
    return this.configService.get<boolean>('APP_DEBUG') || true;
  }

  get rateLimit(): number {
    return this.configService.get<number>('RATE_LIMIT') || 5000;
  }
}
