import {Inject, Injectable} from '@nestjs/common';
import {ConnectWalletInput} from './dto/connect-wallet.input';
import {ClientProxy} from "@nestjs/microservices";
import {PortfolioWalletInput} from "./dto/portfolio-wallet.input";

@Injectable()
export class WalletService {

    constructor(
        @Inject('SERVICES')
        private readonly clientService: ClientProxy,
    ) {
    }

    async connect(connectWalletInput: ConnectWalletInput) {
        return this.clientService.send('connect_wallet', connectWalletInput);
    }

    portfolio(portfolioWalletInput: PortfolioWalletInput) {
        console.log(portfolioWalletInput)
        return this.clientService.send('portfolio_wallet', portfolioWalletInput);
    }
}
