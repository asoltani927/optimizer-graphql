import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class MarketService {

    constructor(@Inject('MARKET_SERVER') private readonly market_server: ClientProxy) { }

    getStatus() {
        return this.market_server.send('market_status', {});
    }
}
