
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class Collection {
    name?: Nullable<string>;
    slug?: Nullable<string>;
    one_day_volume?: Nullable<number>;
    one_day_change?: Nullable<number>;
    one_day_sales?: Nullable<number>;
    one_day_average_price?: Nullable<number>;
    seven_day_volume?: Nullable<number>;
    seven_day_change?: Nullable<number>;
    seven_day_sales?: Nullable<number>;
    seven_day_average_price?: Nullable<number>;
    thirty_day_volume?: Nullable<number>;
    thirty_day_change?: Nullable<number>;
    thirty_day_sales?: Nullable<number>;
    thirty_day_average_price?: Nullable<number>;
    total_sales?: Nullable<number>;
    total_supply?: Nullable<number>;
    count?: Nullable<number>;
    num_owners?: Nullable<number>;
    average_price?: Nullable<number>;
    num_reports?: Nullable<number>;
    market_cap?: Nullable<number>;
    total_volume?: Nullable<number>;
    floor_price?: Nullable<number>;
    percent?: Nullable<number>;
}

export class Chart {
    name?: Nullable<string>;
    slug?: Nullable<string>;
    values?: Nullable<Nullable<number>[]>;
    labels?: Nullable<Nullable<string>[]>;
}

export class ChartInput {
    overtime?: Nullable<string>;
    slug?: Nullable<string>;
}

export abstract class IQuery {
    abstract collections(): Nullable<Collection[]> | Promise<Nullable<Collection[]>>;

    abstract chart(slug?: Nullable<string>, overtime?: Nullable<string>, type?: Nullable<string>): Chart | Promise<Chart>;

    abstract gas(): GasStat | Promise<GasStat>;

    abstract market(): Nullable<State> | Promise<Nullable<State>>;

    abstract TFluctuation(slug?: Nullable<string>, overtime?: Nullable<string>): Chart | Promise<Chart>;
}

export class GasStat {
    fast?: Nullable<number>;
    standard?: Nullable<number>;
    slow?: Nullable<number>;
}

export class State {
    status?: Nullable<number>;
}

export class Wallet {
    wallet_address?: Nullable<string>;
}

export class Portfolio {
    evaluation?: Nullable<number>;
    collections?: Nullable<Nullable<Collection>[]>;
}

export abstract class IMutation {
    abstract connectWallet(wallet_address?: Nullable<string>): Nullable<Wallet> | Promise<Nullable<Wallet>>;

    abstract walletPortfolio(wallet_address?: Nullable<string>, wallet_collections?: Nullable<Nullable<string>[]>, collections_balances?: Nullable<Nullable<number>[]>): Nullable<Portfolio> | Promise<Nullable<Portfolio>>;
}

type Nullable<T> = T | null;
