import {ConfigModule, ConfigService} from '@nestjs/config';
import {join} from "path";
import {GqlModuleOptions} from "@nestjs/graphql/dist/interfaces/gql-module-options.interface";

export const GraphqlConfigAsync = {
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (configService: ConfigService) => <GqlModuleOptions>({
        isGlobal: true,
        // debug: false,
        // playground: false,
        typePaths: [join(process.cwd(), 'src/modules/**/*.graphql')],
        definitions: {
            path: join(process.cwd(), 'src/graphql.ts'),
            outputAs: 'class'
        }
    }),
};
