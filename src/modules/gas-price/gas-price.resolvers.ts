import { Args, Query, Resolver } from '@nestjs/graphql';
import { GasPriceService } from './gas-price.service';

@Resolver('collection')
export class GasPriceResolvers {
  constructor(private readonly service: GasPriceService) {}

  @Query()
  async gas() {
    return this.service.get();
  }
}
