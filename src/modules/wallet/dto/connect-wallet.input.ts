import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class ConnectWalletInput {
  @Field(() => String, { description: 'The user wallet address' })
  wallet_address: string;
}
