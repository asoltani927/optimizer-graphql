import { Overtime } from "../Collection.enum";

export interface CollectionChartDTO{
    overtime: Overtime
    slug: string
}