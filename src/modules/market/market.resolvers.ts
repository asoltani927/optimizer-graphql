import { Query, Resolver } from "@nestjs/graphql";
import { isNumber } from "class-validator";
import { MarketService } from "./market.service";

@Resolver('markets')
export class MarketResolvers {

    constructor(private readonly marketService: MarketService) {

    }

    @Query()
    async market() {
        return await this.marketService.getStatus()
    }
}