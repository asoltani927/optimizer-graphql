import { Module } from '@nestjs/common';
import { WalletService } from './wallet.service';
import { WalletResolver } from './wallet.resolver';
import {ClientsModule} from "@nestjs/microservices";
import {ClientsConfig} from "../../config/clients/clients.config";

@Module({
  imports: [
    ClientsModule.register(ClientsConfig),
  ],
  providers: [WalletResolver, WalletService],
})
export class WalletModule {}
