export interface CollectionDetectedDto{
    name: string
    slug: string
}
