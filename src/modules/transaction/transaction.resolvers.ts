import {Args, Query, Resolver} from '@nestjs/graphql';
import {TransactionService} from './transaction.service';

@Resolver('collection')
export class TransactionResolvers {
    constructor(private readonly transactionService: TransactionService) {
    }

    @Query()
    async TFluctuation(
        @Args('slug') slug,
        @Args('overtime') overtime,
        @Args('type') type,
    ) {
        return this.transactionService.getFluctuationOfTransaction({
            overtime: overtime,
            slug: slug,
        });
    }
}
