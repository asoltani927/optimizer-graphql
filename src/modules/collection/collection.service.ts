import {Inject, Injectable} from '@nestjs/common';
import {ClientProxy} from '@nestjs/microservices';
import {CollectionChartDTO} from './dto/CollectionChartInput.dto';
import {ChartFluctuationQueryDTO} from "../transaction/dto/ChartFluctuationQuery.dto";
import {CollectionDetectedDto} from "./dto/CollectionDetected.dto";

@Injectable()
export class CollectionService {
    constructor(
        @Inject('SERVICES')
        private readonly collection_server: ClientProxy,
    ) {
    }

    findAll() {
        return this.collection_server.send('get_all_collections', {});
    }

    getFloorPriceChart(input: CollectionChartDTO) {
        return this.collection_server.send('get_floor_price_average_chart', input);
    }

    getTotalVolumeChart(input: CollectionChartDTO) {
        return this.collection_server.send('get_total_volume_average_chart', input);
    }

    getPriceAveragesChart(input: ChartFluctuationQueryDTO) {
        return this.collection_server.send('get_price_average_chart', input);
    }

    async detectCollections(collection: CollectionDetectedDto) {
        return this.collection_server.send('detect_collections', collection);
    }
}
