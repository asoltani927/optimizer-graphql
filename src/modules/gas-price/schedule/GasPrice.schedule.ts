import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { GasPriceDTO } from '../dto/gas-price.dto';
import { GasPriceService } from '../gas-price.service';
import {GasPriceGateway} from "../gas-price.gateway";

@Injectable()
export class GasPriceSchedule {
  constructor(private service: GasPriceService, private gateway: GasPriceGateway) {}

  @Cron('*/3 * * * * *', {
    name: 'gas_station_collector',
    // timeZone: process.env.TIMEZONE,
  })
  async trigger() {
    await this.service.get().subscribe((data) => {
      const last_date = new Date(data.created_at);
      const different = (Date.now() - last_date.getTime()) / 1000;
      if (different < 3) {
        const price: GasPriceDTO = {
          slow: data.slow,
          standard: data.standard,
          fast: data.fast,
        };
        this.gateway.ethHandleMessage(price);
      }
    });
  }
}
