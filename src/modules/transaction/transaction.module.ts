import {Module} from '@nestjs/common';
import {ClientsModule} from "@nestjs/microservices";
import {TransactionService} from "./transaction.service";
import {TransactionResolvers} from "./transaction.resolvers";
import {ClientsConfig} from "../../config/clients/clients.config";

@Module({
    imports: [
        ClientsModule.register(ClientsConfig),
    ],
    providers: [TransactionService, TransactionResolvers],
})
export class TransactionModule {
}
