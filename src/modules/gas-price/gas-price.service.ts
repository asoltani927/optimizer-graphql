import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class GasPriceService {
  constructor(
    @Inject('GAS_PRICE_SERVER') private readonly gas_server: ClientProxy,
  ) {}

  get() {
    return this.gas_server.send('get_gas_station', {});
  }
}
