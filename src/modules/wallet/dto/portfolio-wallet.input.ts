import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class PortfolioWalletInput {
    @Field(() => String, { description: 'The user wallet address' })
    wallet_address: string;

    @Field(() => [String], { description: 'The user wallet collections' })
    wallet_collections: Array<string>;

    @Field(() => [Number], { description: 'The user wallet collections' })
    collections_balances: Array<number>;
}
