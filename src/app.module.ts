import {Module} from '@nestjs/common';
import {GraphQLModule} from '@nestjs/graphql';
import {CollectionModule} from './modules/collection/collection.module';
import {ConfigModule} from '@nestjs/config';
import {MarketModule} from './modules/market/market.module';
import {GasPriceModule} from './modules/gas-price/gas-price.module';
import {ScheduleModule} from '@nestjs/schedule';
import {TransactionModule} from './modules/transaction/transaction.module';
import {WalletModule} from './modules/wallet/wallet.module';
import {GraphqlConfigAsync} from "./config/app/graphql.config";
import AppConfig from "./config/app/app.config";
import CorsConfig from "./config/app/cors.config";

@Module({
    imports: [
        ConfigModule.forRoot({isGlobal: true}),
        GraphQLModule.forRootAsync(GraphqlConfigAsync),
        ScheduleModule.forRoot(),
        CollectionModule,
        MarketModule,
        GasPriceModule,
        TransactionModule,
        WalletModule,
    ],
    providers: [
        AppConfig, CorsConfig
    ],
})
export class AppModule {
}
