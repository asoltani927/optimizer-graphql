import {AppGateway} from "../../app.gateway";
import {SubscribeMessage} from "@nestjs/websockets";
import {GasPriceDTO} from "./dto/gas-price.dto";

export class GasPriceGateway extends AppGateway{


    @SubscribeMessage('ethStatToServer')
    ethHandleMessage(data: GasPriceDTO) {
        this.wss.emit('ethStatToClient', data);
    }

}
