import {Module} from '@nestjs/common';
import {CollectionResolvers} from './collection.resolvers';
import {CollectionService} from './collection.service';
import {ClientsModule, Transport} from '@nestjs/microservices';
import {CollectionGateway} from "./collection.gateway";
import {ClientsConfig} from "../../config/clients/clients.config";

@Module({
    imports: [
        ClientsModule.register(ClientsConfig),
    ],
    providers: [CollectionService, CollectionResolvers, CollectionGateway],
})
export class CollectionModule {
}
