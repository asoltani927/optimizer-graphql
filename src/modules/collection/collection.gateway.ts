import {
    SubscribeMessage,
} from '@nestjs/websockets';
import {AppGateway} from "../../app.gateway";
import {CollectionDetectedDto} from "./dto/CollectionDetected.dto";
import {CollectionService} from "./collection.service";
import {Socket} from "socket.io";
import {Injectable} from "@nestjs/common";

@Injectable()
export class CollectionGateway extends AppGateway{


    constructor(private readonly service : CollectionService) {
        super();
    }

    @SubscribeMessage('collectionDetected')
    async collectionDetected(client: Socket,data: CollectionDetectedDto) {
        return this.service.detectCollections(data)
    }
}
