import {Args, Query, Resolver} from '@nestjs/graphql';
import {CollectionService} from './collection.service';

@Resolver('collection')
export class CollectionResolvers {
    constructor(private readonly collectionService: CollectionService) {
    }

    @Query()
    async collections() {
        return this.collectionService.findAll();
    }

    @Query()
    async chart(
        @Args('slug') slug,
        @Args('overtime') overtime,
        @Args('type') type,
    ) {
        switch (type) {
            case 'floor_price':
                return this.collectionService.getFloorPriceChart({
                    overtime: overtime,
                    slug: slug,
                });

            case 'total_volume':
                return this.collectionService.getTotalVolumeChart({
                    overtime: overtime,
                    slug: slug,
                });

            case 'price_average':
                return this.collectionService.getPriceAveragesChart({
                    overtime: overtime,
                    slug: slug,
                });
        }
    }
}
